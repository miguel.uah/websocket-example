const WebSocketServer = require('websocket').server
const http = require('http')

const port = 3000
const allowedList = [
    '192.168.1.36', 
    '127.0.0.1'
]

const  server = http.createServer((req, res) => {
	console.log(`Request for: ${req.url}`)
	res.writeHead(200, { 'Content-Type': 'application/json' })
	res.end(JSON.stringify({
        msg: "Hi world"
    }))
})

server.listen(port, () => {
    console.log(`Server is listening on port: ${port}`)
})

const myWebSocketServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false 
});

const originIsAllowed = (origin) => {
    console.log(origin)

    for(let i = 0 ; i < allowedList.length ; i++){
        if(origin.indexOf(allowedList[i]) !== -1){
            return true
        }
    }
    
    return false;
}

myWebSocketServer.on('request', (req) => {
    if (!originIsAllowed(req.remoteAddress)) {
      req.reject();
      console.log((new Date()) + ' Connection from origin ' + req.origin + ' rejected.');
      return;
    }

    const connection = req.accept('echo-protocol', req.origin);
    console.log((new Date()) + ' Connection accepted.');

    connection.on('message', (message) => {
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
            connection.sendUTF(message.utf8Data);
        }
        else if (message.type === 'binary') {
            console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            connection.sendBytes(message.binaryData);
        }
    });
    connection.on('close', (reasonCode, description) => {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});