const WebSocketClient = require('websocket').client;
 
const client = new WebSocketClient();

const websocketServer = 'ws://192.168.1.40:3000/'
 
client.on('connectFailed', (error) => {
    console.log('Connect Error: ' + error.toString());
});
 
client.on('connect', (connection) => {

    console.log('WebSocket Client Connected');

    connection.on('error', (error) => {
        console.log("Connection Error: " + error.toString());
    });

    connection.on('close', () => {
        console.log('echo-protocol Connection Closed');
    });

    connection.on('message', (message) => {
        if (message.type === 'utf8') {
            console.log("Received: '" + message.utf8Data + "'");
        }
    });
    
    const sendDate = () => {
        if (connection.connected) {

            const date = new Date();

            connection.sendUTF(
                date.toUTCString()
            )

            setTimeout(sendDate, 1000);

        }
    }
    
    sendDate();
});
 
client.connect(websocketServer, 'echo-protocol');